using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelf : MonoBehaviour
{

    public void OnFinished()
    {
        Destroy(this.gameObject);
        GetComponent<PlayerController>().chakraInstance=null;

    }
    
}
