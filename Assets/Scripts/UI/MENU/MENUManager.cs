using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class MENUManager : MonoBehaviour
{
    [SerializeField] Text Enemies_Killed, Enemies_Killed_Record;
    DataSaver jsonData;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject rooms;
    [SerializeField]
    private GameObject hud;
    [SerializeField]
    private GameObject startPanel;
    [SerializeField]
    private Button button;
    public bool isStarted;
    void Start()
    {
        string path = Application.persistentDataPath + "/score.json";

        string jsonString;
        if (File.Exists(path))
        {
            jsonString = File.ReadAllText(path);
            jsonData = JsonUtility.FromJson<DataSaver>(jsonString);
            if (jsonData == null) jsonData = new DataSaver(0, 0, 0);
        }
        else
        {
            File.Create(path);
            jsonData = new DataSaver(0, 0, 0);
        }

        Enemies_Killed.text = jsonData.enemies_killed.ToString();
        Enemies_Killed_Record.text = jsonData.enemies_killed_record.ToString();




        hud.SetActive(false);
        isStarted = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void StartGame()
    {
       
      
        isStarted = true;
        var random_room = Random.Range(0, 3);
        Debug.Log(random_room);
        Debug.Log("fhquifqifuhq");
        var spawn_location = rooms.GetComponent<RoomRandom>().rooms[random_room].position;
       
        player.transform.position = spawn_location;  
        startPanel.SetActive(false);
        Debug.Log("fhquifqifuhq");
        button.gameObject.SetActive(false);
        hud.SetActive(true); 
        rooms.GetComponent<RoomRandom>().RoomSpwaner(random_room);
    }


    public void ClickToStart()
    {
        StartGame();
    }

}
