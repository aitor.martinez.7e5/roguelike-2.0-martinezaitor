using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class HUDManager : MonoBehaviour
{
    public int enemy_killed;
    
    public int coins;
    [SerializeField]
    private Text num_kills;
    [SerializeField]
    private Text num_coins;
    [SerializeField]
    private GameObject[] hearts;
    [SerializeField]
    private Sprite[] sprites;
    [SerializeField] private PlayerController pc;

    
    // Start is called before the firdst frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        HP();
        Coins();
        ShowKills();
    }
    
   
    public void HP()
    {
        var heartsstart = hearts.Length;
        for (int i = 0; i < heartsstart; i++)
        {
            hearts[i].SetActive(false);
        }
        for (int j = 0; j < pc.maxhearts; j++)
        {
            hearts[j].SetActive(true);
           

        }

        for (int k = 0; k < heartsstart; k++)
        {
            hearts[k].GetComponent<SpriteRenderer>().sprite = sprites[0];
        }

        for (int l = 0; l < pc.hp; l++)
        {
            hearts[l].GetComponent<SpriteRenderer>().sprite = sprites[1];
        }

    }

   

    public void Heal()
    {


       // hearts[].SetActive(false);


    }

    public void Coins()
    {
        num_coins.text = $"{coins}";
    }

   

    public void GetCoins()
    {
        coins += 10;
    }

    public void GetKills()
    {
        enemy_killed ++;
 

    }
    public void ShowKills()
    {
        num_kills.text = "Totat Enemies Killed: "+$"{enemy_killed}";

    }


}
