using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class Inventory_Controller : MonoBehaviour
{
    public List<GameObject> items = new List<GameObject>();
    public int InventoryIndex;
    public GameObject[] scrolls;

   
 
    // Start is called before the first frame update

     void Awake()
    {
        InventoryIndex=0;
    }


    void Start()
    {
     

    }

    // Update is called once per frame
    void Update()
    {
        items[InventoryIndex].SetActive(true);
      
        //para atras
        if (Input.GetAxis("Mouse ScrollWheel") > 0&&InventoryIndex!=items.Count)
        {
            items[InventoryIndex].SetActive(false);
            items[InventoryIndex+1].SetActive(true);



        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0 && InventoryIndex + 1> items.Count)
        {
            items[InventoryIndex].SetActive(false);
            InventoryIndex = 0;
            items[InventoryIndex].SetActive(true);

        }

        //para adelante
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && InventoryIndex !=0)
        {
            items[InventoryIndex].SetActive(false);
            items[InventoryIndex - 1].SetActive(true);


        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0 && InventoryIndex == 0)
        {
            items[InventoryIndex].SetActive(false);
            InventoryIndex = items.Count;
            items[InventoryIndex].SetActive(true);

        }




    }
}
