using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.RuleTile.TilingRuleOutput;

public class MaskedNinjaController : MonoBehaviour
{
    private HUDManager hudManager;
    private PlayerController playerController;
    [SerializeField] private GameObject[] drops;
    [SerializeField] private GameObject shuriken_position;
    private Rigidbody2D rb;
    private Animator an;
    [SerializeField]
    private float enemy_speed = 2f;
    private float enemy_hp = 50;
    [SerializeField] private float shootcooldown;
    private float _cooldown;
    [SerializeField] private float _bulletSpeed;
    [SerializeField] private GameObject shuriken;
    [SerializeField] private Slider hp;
    [SerializeField]
    private float bulletForce;
    private float attack_size;
    private GameObject player;
    private bool canShoot;
    private float attack_speed;
    private GameObject clone;
    private BoxCollider2D playerboxcollider;




    private void Start()
    {

        attack_size = 1f;
        attack_speed = 2f;
        _cooldown = shootcooldown;
        rb = GetComponent<Rigidbody2D>();
        an = GetComponent<Animator>();
        canShoot = true;
        playerboxcollider = GameObject.Find("Player").GetComponent<BoxCollider2D>();
    }

    private void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        clone = GameObject.Find("Clone");
        HP();
        Die();
        Shoot();
       
    }


    private void Shoot()
    {
       
        

        if (canShoot)
        {


            Debug.Log("eyyyyy");
            Vector3 distance = player.transform.position - transform.position;
            an.SetBool("CanShoot", true);
            var shoot = Instantiate(shuriken, new Vector3(transform.position.x + 0.2f , transform.position.y, transform.position.z), transform.rotation);
            shoot.transform.localScale *= attack_size;
            shoot.GetComponent<Rigidbody2D>().AddForce(distance, ForceMode2D.Impulse);
            an.SetBool("CanShoot", false);
            //shoot.transform.up = Vector2.down;
            StartCoroutine(ShootCooldown());

        }

    }

    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(attack_speed);
        canShoot = true;
    }





    void HP()
    {
        hp.value = enemy_hp;
    }
   
    
    
    
    void Die()
    {
        if (enemy_hp <= 0)
        {
            AudioFXManager.Instance.PlaySound(2);
            an.SetBool("isDead", true);
        }

    }

    public void Destroy()
    {
        an.SetBool("isDead", false);
        GameObject.Find("HUD").GetComponent<HUDManager>().GetKills();
        GameObject.Find("HUD").GetComponent<HUDManager>().GetCoins();
        GameObject.Find("Player").GetComponent<PlayerController>().enemy_count++;
        Drop();
        
    }
   
 

   

  

    void Drop()
    {
        var _drop = Random.Range(1, 100);
        if(_drop < 60)
        {
            var drop = Instantiate(drops[0], transform.position, Quaternion.identity);
            
            Destroy(this.gameObject);
        }
        else if (_drop < 90 && _drop > 60)
        {
            var drop = Instantiate(drops[1], transform.position, Quaternion.identity);
           
            Destroy(this.gameObject);
        }
        else if(_drop > 90)
        {
            var drop = Instantiate(drops[2], transform.position, Quaternion.identity);
           
            Destroy(this.gameObject);
        }

       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Shuriken"))
        {
            AudioFXManager.Instance.PlaySound(1);
            enemy_hp -= 20;
            Destroy(collision.gameObject);
        } 
        
        if (collision.gameObject.CompareTag("Jutsu"))
        {
            AudioFXManager.Instance.PlaySound(1);
            enemy_hp -= 55;
            Destroy(collision.gameObject);
        }


    }

}
