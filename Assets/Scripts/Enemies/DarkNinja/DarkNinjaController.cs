using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DarkNinjaController : MonoBehaviour
{
    private HUDManager hudManager;
    private PlayerController playerController;
    private Rigidbody2D rb;
    private Animator an;
    [SerializeField]
    private float enemy_speed = 2f;
    private float enemy_hp = 60;
    private GameObject player;
    [SerializeField] private GameObject[] drops;
    [SerializeField] private Slider hp;
    private GameObject clone;
    private BoxCollider2D playerboxcollider;
    // Start is called before the first frame update
    void Start()
    {
       rb=GetComponent<Rigidbody2D>(); 
       an=GetComponent<Animator>();
       
       playerboxcollider = GameObject.Find("Player").GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {

        clone = GameObject.Find("Clone");
        
            HP();
            EnemyMovement();
            Die();
        
        
            
    }

    void HP()
    {
        hp.value = enemy_hp;
    }
    void Die()
    {
        if (enemy_hp <= 0)
        {
            AudioFXManager.Instance.PlaySound(2);
           
            an.SetBool("isDead", true);
        }
        
    }

    public void Destroy()
    {
        an.SetBool("isDead", false);
        GameObject.Find("HUD").GetComponent<HUDManager>().GetKills();
        GameObject.Find("HUD").GetComponent<HUDManager>().GetCoins();
        GameObject.Find("Player").GetComponent<PlayerController>().enemy_count++;

        Drop();

    }







    void Drop()
    {
        var _drop = Random.Range(1, 100);
        if (_drop < 60)
        {
            var drop = Instantiate(drops[0], transform.position, Quaternion.identity);
            
            Destroy(gameObject);
        }
        else if (_drop < 90 && _drop > 60)
        {
            var drop = Instantiate(drops[1], transform.position, Quaternion.identity);
            
            Destroy(gameObject);

        }
        else if (_drop > 90)
        {
            var drop = Instantiate(drops[2], transform.position, Quaternion.identity);
            
            Destroy(gameObject);

        }


    }
    void EnemyMovement()
    {       player = GameObject.FindGameObjectWithTag("Player");
            var direccion = player.transform.position-transform.position    ;
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, enemy_speed * Time.deltaTime);
            an.SetFloat("MovimientoX",direccion.x);
            an.SetFloat("MovimientoY", direccion.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Shuriken"))
        {
            enemy_hp -= 20;
            AudioFXManager.Instance.PlaySound(1);
            Destroy(collision.gameObject);
        }else if (collision.gameObject.CompareTag("Jutsu"))
        {
            AudioFXManager.Instance.PlaySound(1);
            enemy_hp -= 55;
            Destroy(collision.gameObject);
        }

    }
}
