using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class MarkController : MonoBehaviour
{

    public GameObject[] enemies;
    

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Spawner", 2f);
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
      
           

        
    }

    private void Spawner()
    {
        var instance = Instantiate(enemies[Random.Range(0,enemies.Length)], transform.position, Quaternion.identity);
        
    }

    public void Destroy()
    {
        Destroy(gameObject);        
    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            Destroy(gameObject);
        }
    }

    
}
