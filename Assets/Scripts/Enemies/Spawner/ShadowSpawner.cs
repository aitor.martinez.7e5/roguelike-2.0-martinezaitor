using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowSpawner : MonoBehaviour
{

    [SerializeField] private GameObject smoke_Spawner;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SpawnSmoke()
    {
        Instantiate(smoke_Spawner,transform.position,Quaternion.identity);
        Destroy(gameObject);
    }
}
