using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RoomRandom : MonoBehaviour
{

    public Transform[] rooms;
    public GameObject room1;
    public GameObject room2;
    public GameObject room3;
    public GameObject shop;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       
       var room_num = Random.Range(0, rooms.Length);
        GameObject.Find("Player").transform.position = rooms[room_num].position;
        RoomSpwaner(room_num);
        room1.GetComponent<Room1>().door.SetActive(false);
        room2.GetComponent<Room2>().door.SetActive(false);
        room3.GetComponent<Room3>().door.SetActive(false);
    }

    public void RoomSpwaner(int num)
    {
        switch (num)
        {
            case 0:
                room1.GetComponent<Room1>().CanSpawnRoom1=true;
                break;

                case 1:
                room2.GetComponent<Room2>().CanSpawnRoom2 = true;
                break;

            case 2:
                room3.GetComponent<Room3>().CanSpawnRoom3 = true;
                break;

            case 3:

                room1.GetComponent<Room1>().CanSpawnRoom1 = false;
                room2.GetComponent<Room2>().CanSpawnRoom2 = false;
                room3.GetComponent<Room3>().CanSpawnRoom3 = false;
                break;
              



        }
    }
}
