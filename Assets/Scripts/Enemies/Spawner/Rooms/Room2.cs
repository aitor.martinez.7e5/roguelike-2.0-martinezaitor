using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room2 : MonoBehaviour
{
    public GameObject player;
    [SerializeField] public GameObject door;
    [SerializeField]
    private GameObject spawner_mark;
    [SerializeField]
    private float spawnTime = 1f;
    private float limit_enemies;
    private int enemy_counter = 0;
    private bool onCooldown;
    public bool CanSpawnRoom2;
    public delegate void clearfloor();
    public event clearfloor KillAllEnemies;


    // Start is called before the first frame update
    void Start()
    {
        onCooldown = false;
        door.SetActive(false);
        CanSpawnRoom2 = false;
        limit_enemies = Random.Range(0,10);

    }

    private void Update()
    {


        if (onCooldown == false && enemy_counter != limit_enemies&& CanSpawnRoom2 == true)
        {
            float newX = Random.Range(-92.84f, -71.7f);
            float newY = Random.Range(37f, 37.21f);
            var instance1 = Instantiate(spawner_mark, new Vector3(newX, newY, 0), Quaternion.identity);
            enemy_counter++;

            onCooldown = true;

            Invoke("Cooldown", spawnTime);


        }

        if (player.GetComponent<PlayerController>().enemy_count == limit_enemies)
        {
            Debug.Log("papaya");
            door.SetActive(true);
            Debug.Log("papaya2");
            player.GetComponent<PlayerController>().enemy_count = 0;
        }


    }

    private void Cooldown()
    {
        onCooldown = false;

    }

}
