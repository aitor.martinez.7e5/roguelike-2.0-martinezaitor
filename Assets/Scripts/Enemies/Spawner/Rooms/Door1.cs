using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door1 : MonoBehaviour
{
    public DoorStates doorState;
    public GameObject door;
    BoxCollider2D BoxCollider2D;


   public enum DoorStates
    {
        closed,
        open
    }
    // Start is called before the first frame update
    void Start()
    {
        BoxCollider2D = GetComponent<BoxCollider2D>();
        BoxCollider2D.enabled = false;
 
        doorState = DoorStates.closed;
        
    }

   

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDisable()
    {
       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            
        }
    }

}
