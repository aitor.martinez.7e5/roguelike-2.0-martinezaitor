using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart_Controller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameObject.Find("Player").GetComponent<PlayerController>().maxhearts++;
            Destroy(this.gameObject);

            // GameObject.Find("HUD").GetComponent<HUDManager>().IncreaseScore(40);
        }
    }
}
