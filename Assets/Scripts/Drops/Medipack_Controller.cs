using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medipack_Controller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (GameObject.Find("Player").GetComponent<PlayerController>().hp < GameObject.Find("Player").GetComponent<PlayerController>().maxhearts)
            {
                GameObject.Find("Player").GetComponent<PlayerController>().hp++;
                Destroy(this.gameObject);
            }
            else
            {
                Destroy(this.gameObject);
            }


            // GameObject.Find("HUD").GetComponent<HUDManager>().IncreaseScore(40);
        }
    }
}
