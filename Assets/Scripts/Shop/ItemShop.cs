using TMPro;
using UnityEngine;

public class ItemShop : MonoBehaviour
{
    private GameObject item;
    public string itemName;
    public int arraynum;
    public int price;
    public TMP_Text nameText;
    private TMP_Text priceText;

    public void Awake()
    {

        nameText = transform.GetChild(0).GetComponent<TMP_Text>();
        priceText = transform.GetChild(1).GetComponent<TMP_Text>();
    }

    public void Start()
    {
        item = GameObject.Find(itemName);
        item.SetActive(false);
        nameText.text = itemName;
        priceText.text = $"Price: {price}";
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player") && GameObject.Find("HUD").GetComponent<HUDManager>().coins>=price)
        {
            item.SetActive(true);
            item.GetComponent<Inventory_Controller>().items.Add(item);   
            
            GameObject.Find("HUD").GetComponent<HUDManager>().coins-=price;
            
            
        }
    }
}