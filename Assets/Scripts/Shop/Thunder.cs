using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Thunder : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player") )
        {

            GameObject.Find("Player").GetComponent<PlayerController>().canThunderJutsu = true;
            Destroy(gameObject);

        }
    }

}
