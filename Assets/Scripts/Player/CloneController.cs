using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneController : MonoBehaviour
{
    [SerializeField] GameObject Smoke;
    private Animator an;
    // Start is called before the first frame update
    void Start()
    {
        
        an = GetComponent<Animator>();
       
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Left()
    { 
        an.SetBool("Right", false);
        an.SetBool("Left", true);
       
    }

    public void Right()
    {
        an.SetBool("Right", true);
        an.SetBool("Down", false);
    }
    public void Up()
    {
        an.SetBool("Down", true);
        an.SetBool("Up", false);
    }
    public void Down()
    {
        an.SetBool("Up", true);
        an.SetBool("Idle", false);
    }


    public void CloneDestroy()
    {
        var smoke = Instantiate(Smoke, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
