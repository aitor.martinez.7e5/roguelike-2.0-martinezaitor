using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Jutsu", menuName = "Jutsu")]
public class JutsuData : ScriptableObject
{
    public float chakra;
    public GameObject Scroll;

}
