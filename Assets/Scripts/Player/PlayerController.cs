using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static UnityEditor.Experimental.GraphView.GraphView;
using static UnityEngine.GraphicsBuffer;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerController : MonoBehaviour
{
    
    public int enemy_count;
    [SerializeField]private Camera camera;
    [SerializeField]private float velocity;
    [SerializeField]GameObject shuriken;
    [SerializeField] GameObject Smoke;
    [SerializeField] GameObject Clone;
    [SerializeField] GameObject fireJutsu;
    [SerializeField] GameObject iceJutsu;
    [SerializeField] GameObject thunderJutsu;
    [SerializeField] GameObject rockJutsu;
    [SerializeField] GameObject plantJutsu;
    [SerializeField] private Slider chakra_slider;
    [System.NonSerialized]
    public Inventory Inventory;
    public Jutsu CurrentJutsu;



    [SerializeField] GameObject Chakra;
    private Vector3 target;
    public float chakra= 0;
    private float shurikenForce = 20;
    private bool canShoot;
    private float attack_speed = 1;
    private float attack_size = 1;
    public float attack_damage = 1;
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    public float movimientoX;
    public float movimientoY;
    public int maxhearts = 2;
    public int hp = 2;
    private Animator an;
    public bool canClone;
    public float CloneSpeed = 6f;
    private float cloneCooldown = 20;
    private float CloneTimer = 0;
    public bool canJutsu;
    public bool canRockJutsu;
    public bool canFireJutsu;
    public bool canIceJutsu;
    public bool canPlantJutsu;
    public bool canThunderJutsu;
    public float JutsuCooldown = 30;
    public float JutsuTimer = 0;
    Vector3 mousePos;
    private BoxCollider2D box;

    public GameObject chakraInstance;
    
    private void Awake()

    {
       
    }


    // Start is called before the first frame update
    void Start()
    {
        canJutsu = true;
        canShoot = true;
        canClone = true;
        sr = GetComponent<SpriteRenderer>();
        an = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        box = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        Move();
        Die();
        CloneJutsu();
        Jutsus();
        Shoot();
        Fight();
        ChargeChakra();
        ChakraBar();

    }

    void FixedUpdate()
    {
       /* if (GameObject.Find("MENU").GetComponent<MENUManager>().isStarted)
        {
         Move();
           
        }*/
        
        if (CloneTimer >= cloneCooldown)
        {
            canClone = true;
        }
        if (JutsuTimer >= JutsuCooldown)
        {
            canJutsu = true;
        }

    }

    void Move()
    {
        
           
                movimientoX = Input.GetAxisRaw("Horizontal");
                movimientoY = Input.GetAxisRaw("Vertical");
                rb.velocity = new Vector2(movimientoX * velocity, movimientoY * velocity);
                rb.velocity.Normalize();
                if (rb.velocity != Vector2.zero)
                    {
                        an.SetFloat("MovimientoX", movimientoX);
                        an.SetFloat("MovimientoY", movimientoY);
                        an.SetBool("isWalking", true);
                    }
                else
                    {
                        an.SetBool("isWalking", false);
                    }

                
            
        
    }

    void Fight()
    {
        if (Input.GetMouseButtonDown(1))
        {
            an.SetBool("isAttacking", true);

        }
        


    }



    void stopfighting()
    {
        an.SetBool("isAttacking", false);
    }
    
    void ChargeChakra()
    {
        if (Input.GetKey(KeyCode.C)&&chakra<=100)
        {
            an.SetBool("IsChargingChakra", true);
            if (chakraInstance == null)
            {
                chakraInstance = Instantiate(Chakra, transform.position, Quaternion.identity);
                chakra += Time.deltaTime * 150   ;
                chakraInstance.transform.parent = gameObject.transform;
            }

        }            

        else
        {
            an.SetBool("IsChargingChakra", false);
            //chakraInstance.GetComponent<Animator>().SetBool("isCharging", false);
        }
       
    }


    void Jutsus()
    {
        if (canJutsu&&canFireJutsu)
        {
            if(Input.GetKey(KeyCode.Alpha1))
            {
                box.isTrigger = true;
               
                an.SetBool("FireJutsu", true);
            }


        }

        if (canJutsu && canIceJutsu)
        {
            if (Input.GetKey(KeyCode.Alpha2))
            {
                box.isTrigger = true;

                an.SetBool("FireJutsu", true);
            }


        }

        if (canJutsu && canPlantJutsu)
        {
            if (Input.GetKey(KeyCode.Alpha3))
            {
                box.isTrigger = true;

                an.SetBool("FireJutsu", true);
            }


        }

        if (canJutsu && canThunderJutsu)
        {
            if (Input.GetKey(KeyCode.Alpha4))
            {
                box.isTrigger = true;

                an.SetBool("FireJutsu", true);
            }


        }

        if (canJutsu && canRockJutsu)
        {
            if (Input.GetKey(KeyCode.Alpha5))
            {
                box.isTrigger = true;

                an.SetBool("FireJutsu", true);
            }


        }



        JutsuTimer += Time.deltaTime; //Makes timer increase
        if (JutsuTimer >= JutsuCooldown)
        {
            JutsuTimer = JutsuCooldown; //Stops the timer
        }


    }
    


    void CloneJutsu()
    {
        if (canClone&&Input.GetKey(KeyCode.Space))
        {

        sr.color = new Color(1,1, 1,0.15f) ;
        gameObject.tag = "Clone";
        box.isTrigger = true;
        an.SetBool("CloneJutsu", true); 

        }
            
        CloneTimer += Time.deltaTime; //Makes timer increase
        if (CloneTimer >= cloneCooldown)
        {
            CloneTimer = cloneCooldown; //Stops the timer
        }
    }
    void Shoot()
    {
        
        if (canShoot)
        {        
            target = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
            Vector3 difference = target - transform.position;
           
            if (Input.GetMouseButtonDown(0))
            {
                float distance = difference.magnitude;
                Vector2 direction = difference / distance;
                direction.Normalize();
                an.SetBool("isAttacking", true);
                var shoot = Instantiate(shuriken, transform.position, Quaternion.identity);
                shoot.transform.position = transform.position;
                shoot.GetComponent<Rigidbody2D>().velocity = direction * shurikenForce;

                StartCoroutine(ShootCooldown());

            }


        }
    }

    public void DidFireJutsu()
    {
        an.SetBool("FireJutsu", false);

        
        var fire = Instantiate(fireJutsu, transform.position, Quaternion.identity);
        box.isTrigger = false;  
        canFireJutsu = false;
       
        //box.isTrigger = false;
        JutsuTimer = 0.0f;
    }

    public void DidCloneJutsu()
    {
         an.SetBool("CloneJutsu", false);
        var clone = Instantiate(Clone, transform.position, Quaternion.identity);
        var smoke = Instantiate(Smoke, transform.position, Quaternion.identity);
        transform.position += (mousePos- transform.position).normalized * CloneSpeed;
        box.isTrigger = false;
        canClone = false;
        CloneTimer = 0.0f;
        StartCoroutine(CloneCooldown());
    }
   private IEnumerator CloneCooldown()
    {
       
        yield return new WaitForSeconds(2.5f);
        sr.color = new Color(1, 1, 1, 1); 
         gameObject.tag = "Player";
        
        
    }

    private IEnumerator ShootCooldown()
    {        
        an.SetBool("isAttacking", false);
        canShoot = false;
        yield return new WaitForSeconds(attack_speed);
        canShoot = true;
    }

    public void ChakraBar()
    {
        chakra_slider.value = chakra;
    }

    void Die()
    {
        if (hp <= 0)
        {
            AudioFXManager.Instance.PlaySound(2);
            SceneManager.LoadScene("Game");
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            AudioFXManager.Instance.PlaySound(1);
            hp--;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Shuriken_Enemy"))
        {
            AudioFXManager.Instance.PlaySound(1);
             hp--;
            Destroy(collision.gameObject);
        }
    }

  

}


