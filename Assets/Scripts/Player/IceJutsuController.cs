using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceJutsuController : MonoBehaviour
{

    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent <Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Threw()
    {
        anim.SetBool("isCharged", true);
    }


    
    public void Destroy()
    {
        Destroy(gameObject);
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy") || collision.collider.CompareTag("Wall")) {
            {
                anim.SetBool("hasHit", true);
            }
        }
    }
}
